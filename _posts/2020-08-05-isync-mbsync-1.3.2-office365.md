---
layout: post
title: isync/mbsync and office 365
comments: false
categories:
  - mail
tags:
  - mail
---

I use `mbsync` to synchronise email between Email Server and my computer.
I have an email address hosted by Office 365.

One day, it stops synchronising email in my INBOX with:

> Maildir warning: ignoring INBOX in /patch/to/maildir

It seems like from that version, `INBOX` starts to get treated specially by
mbsync (it's already special in maildir specification, I think).

I need to add an `Inbox` directive to `.mbsyncrc` to make mbsync work again.

	IMAPAccount mail@example.com
	Host outlook.office365.com
	User mail@example.com
	PassCmd "pass mail@example.com"
	SSLType IMAPS
	AuthMechs LOGIN

	IMAPStore mail@example.com-remote
	Account mail@example.com

	MaildirStore mail@example.com-local
	Path ~/.cache/maildir/example/
	Inbox ~/.cache/maildir/example/Inbox
	SubFolders Verbatim

	Channel mail@example.com
	Master :mail@example.com-remote:
	Slave :mail@example.com-local:
	Patterns * !Calendar\ Alert !Notes !Outbox !Drafts

**Note:** the last element in Inbox must be anything but `INBOX`.

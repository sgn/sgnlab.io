---
layout: post
title: Bash - Check for missing newline
comments: false
categories:
  - tips
tags:
  - bash
  - commandline
---

Simply:

```sh
diff -u <(cat "$file"; echo '') "$file" | grep -q '\\'
```

Will do it.

because diff(1) will generate:

> \ No newline at end of file

if the diff (and/or context) includes the last line of file,
and the line isn't terminated by newline.

---

Someone suggest simpler code and it only requires POSIX sh

```sh
while IFS= read -r line; do : ; done <"$file"
test -n "$line" && echo missing newline
```

---

Simplest solution:

```sh
test -z "$(tail -c 1 "$file")"
```
